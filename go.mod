module gitlab.com/peterbozso/peterbozso.gitlab.io

go 1.23.3

require (
	github.com/alecthomas/chroma/v2 v2.14.0
	github.com/fsnotify/fsnotify v1.8.0
	github.com/gorilla/feeds v1.2.0
	github.com/tdewolff/minify/v2 v2.21.2
	github.com/yuin/goldmark v1.7.8
	github.com/yuin/goldmark-highlighting/v2 v2.0.0-20230729083705-37449abec8cc
	go.abhg.dev/goldmark/anchor v0.1.1
)

require (
	github.com/dlclark/regexp2 v1.11.0 // indirect
	github.com/tdewolff/parse/v2 v2.7.19 // indirect
	golang.org/x/sys v0.25.0 // indirect
)
