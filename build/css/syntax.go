package css

import (
	"bytes"
	"fmt"
	"log"
	"strings"

	"github.com/alecthomas/chroma/v2"
	chromahtml "github.com/alecthomas/chroma/v2/formatters/html"
	chromastyles "github.com/alecthomas/chroma/v2/styles"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

func generateSyntaxHighlight(sb *strings.Builder) {
	generateStyle(sb, "light", config.SyntaxHighlightStyleLight)
	generateStyle(sb, "dark", config.SyntaxHighlightStyleDark)
	writeln(sb, ".chroma .line { display: inline; }")
	writeln(sb, "")
}

func generateStyle(sb *strings.Builder, colorScheme string, styleName string) {
	style := getStyle(styleName)
	background := getBackground(style)
	writeln(sb, fmt.Sprintf("@media (prefers-color-scheme: %s) {", colorScheme))
	writeln(sb, fmt.Sprintf("code { background-color: %s; }", background))
	writeln(sb, fmt.Sprintf("%s}", asString(style)))
}

func getStyle(styleName string) *chroma.Style {
	builder := chromastyles.Get(styleName).Builder()
	style, err := builder.Build()
	if err != nil {
		log.Panic(err)
	}
	return style
}

func getBackground(style *chroma.Style) string {
	return style.Get(chroma.Background).Background.String()
}

func asString(style *chroma.Style) string {
	var buf bytes.Buffer
	formatter := chromahtml.New(chromahtml.WithClasses(true))
	err := formatter.WriteCSS(&buf, style)
	if err != nil {
		log.Panic(err)
	}
	return buf.String()
}
