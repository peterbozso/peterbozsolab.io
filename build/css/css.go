package css

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

func Build() {
	var sb strings.Builder
	generateSyntaxHighlight(&sb)
	mergeStyles(&sb)
	save(&sb)
}

func mergeStyles(sb *strings.Builder) {
	stylesDir := filepath.FromSlash("build/css/styles")

	files, err := os.ReadDir(stylesDir)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if filepath.Ext(file.Name()) != ".css" {
			continue
		}

		path := filepath.Join(stylesDir, file.Name())

		buf, err := os.ReadFile(path)
		if err != nil {
			log.Panic(err)
		}

		writeln(sb, string(buf))
	}
}

func writeln(sb *strings.Builder, line string) {
	_, err := fmt.Fprintln(sb, line)
	if err != nil {
		log.Panic(err)
	}
}

func save(sb *strings.Builder) {
	file, err := os.Create(filepath.Join(config.OutputDir, "style.css"))
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	_, err = file.WriteString(sb.String())
	if err != nil {
		log.Panic(err)
	}
}
