package posts

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"strings"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/text"
)

type Post struct {
	URL     string // Relative to config.BlogURL
	Title   string
	Date    string
	Content template.HTML
}

// Returns posts in descending order of publication date.
func Parse(md goldmark.Markdown) []Post {
	postsDir := filepath.FromSlash("content/posts")

	files, err := os.ReadDir(postsDir)
	if err != nil {
		log.Fatal(err)
	}

	var posts []Post

	for _, file := range files {
		path := filepath.Join(postsDir, file.Name())

		post, err := getPost(path, md)
		if err == nil {
			posts = append(posts, post)
		} else {
			log.Printf("An error occurred while parsing a post (%s): %s", filepath.Base(path), err)
		}
	}

	slices.Reverse(posts)
	return posts
}

func getPost(path string, md goldmark.Markdown) (Post, error) {
	err := validateFilename(path)
	if err != nil {
		return Post{}, err
	}

	title, content, err := parseMD(path, md)
	if err != nil {
		return Post{}, err
	}

	return Post{
		URL:     getURL(path),
		Title:   title,
		Date:    getDate(path),
		Content: content,
	}, nil
}

func validateFilename(path string) error {
	re := regexp.MustCompile(`^\d{4}-\d{2}-\d{2}-[a-zA-Z0-9-]+\.md$`)

	if re.MatchString(filepath.Base(path)) {
		return nil
	} else {
		return fmt.Errorf("invalid file name")
	}
}

func parseMD(path string, md goldmark.Markdown) (title string, content template.HTML, err error) {
	mdBuf, err := os.ReadFile(path)
	if err != nil {
		log.Panic(err)
	}

	doc := md.Parser().Parse(text.NewReader(mdBuf))

	titleHeading, err := getTitleHeading(doc)
	if err != nil {
		return "", "", err
	}
	doc.RemoveChild(doc, titleHeading)

	var htmlBuf bytes.Buffer
	err = md.Renderer().Render(&htmlBuf, mdBuf, doc)
	if err != nil {
		log.Panic(err)
	}

	return string(titleHeading.Lines().Value(mdBuf)), template.HTML(htmlBuf.String()), nil
}

func getTitleHeading(doc ast.Node) (*ast.Heading, error) {
	titleHeading, isHeading := doc.FirstChild().(*ast.Heading)
	if isHeading && titleHeading.Level == 1 {
		return titleHeading, nil
	}
	return nil, fmt.Errorf("doesn't contain level 1 header")
}

func getDate(path string) string {
	return filepath.Base(path)[0:10]
}

func getURL(path string) string {
	fileName := strings.TrimSuffix(filepath.Base(path), filepath.Ext(path))
	slug := fileName[11:] // Without the date.
	return filepath.Join("posts", slug)
}
