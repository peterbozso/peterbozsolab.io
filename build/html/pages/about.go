package pages

import (
	"bytes"
	"html/template"
	"log"
	"os"
	"path/filepath"

	"github.com/yuin/goldmark"
)

func About(md goldmark.Markdown) Page {
	fileBuf, err := os.ReadFile(filepath.FromSlash("content/about.md"))
	if err != nil {
		log.Panic(err)
	}

	var htmlBuf bytes.Buffer
	err = md.Convert(fileBuf, &htmlBuf)
	if err != nil {
		log.Panic(err)
	}

	content := template.HTML(htmlBuf.String())

	return NewPage("About", content, "about", "about")
}
