package pages

func NotFound() Page {
	return NewPage("Page not found", nil, "404", "404")
}
