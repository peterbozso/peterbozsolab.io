package pages

import "gitlab.com/peterbozso/peterbozso.gitlab.io/build/posts"

func Index(posts []posts.Post) Page {
	return NewPage("", posts, "index", "index")
}
