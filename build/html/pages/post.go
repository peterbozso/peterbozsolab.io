package pages

import (
	"html/template"

	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/posts"
)

func Post(post posts.Post) Page {
	data := struct {
		Title   string
		Date    string
		Content template.HTML
	}{
		Title:   post.Title,
		Date:    post.Date,
		Content: post.Content,
	}

	return NewPage(post.Title, data, "post", post.URL)
}
