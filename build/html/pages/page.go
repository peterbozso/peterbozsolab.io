package pages

import (
	"path/filepath"

	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

type Page struct {
	Title      string
	Data       any
	TmplPath   string
	OutputPath string
}

// tmplFile is relative to build/html/pages, and it is only a filename without extension.
// outputPath is relative to config.OutputDir, and it can be a sub-path without file extension.
func NewPage(title string, data any, tmplFile string, outputPath string) Page {
	return Page{
		Title:      title,
		Data:       data,
		TmplPath:   filepath.Join(filepath.FromSlash("build/html/pages"), tmplFile+".tmpl"),
		OutputPath: filepath.Join(config.OutputDir, outputPath+".html"),
	}
}
