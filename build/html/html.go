package html

import (
	"bytes"
	"html/template"
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/yuin/goldmark"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/html/pages"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/posts"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

func Build(posts []posts.Post, md goldmark.Markdown) {
	createPostsDir()
	for _, p := range posts {
		render(pages.Post(p))
	}

	render(pages.Index(posts))
	render(pages.About(md))
	render(pages.NotFound())
}

func createPostsDir() {
	err := os.MkdirAll(filepath.Join(config.OutputDir, "posts"), 0777)
	if err != nil {
		log.Panic(err)
	}
}

func render(page pages.Page) {
	data := struct {
		BlogTitle string
		PageTitle string
		Content   template.HTML
	}{
		BlogTitle: config.BlogTitle,
		PageTitle: getTitle(page.Title),
		Content:   getContent(page.TmplPath, page.Data),
	}

	file, err := os.Create(page.OutputPath)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	executeTmpl(filepath.FromSlash("build/html/layout.tmpl"), data, file)
}

func getTitle(title string) string {
	if title == "" {
		return config.BlogTitle
	} else {
		return title + " | " + config.BlogTitle
	}
}

func getContent(tmplPath string, data any) template.HTML {
	var buf bytes.Buffer
	executeTmpl(tmplPath, data, &buf)
	return template.HTML(buf.String())
}

func executeTmpl(path string, data any, output io.Writer) {
	tmpl, err := template.ParseFiles(path)
	if err != nil {
		log.Panic(err)
	}

	err = tmpl.Execute(output, data)
	if err != nil {
		log.Panic(err)
	}
}
