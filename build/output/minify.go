package output

import (
	"io/fs"
	"log"
	"os"
	"path/filepath"

	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/css"
	"github.com/tdewolff/minify/v2/html"
	"github.com/tdewolff/minify/v2/svg"
	"github.com/tdewolff/minify/v2/xml"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

func Minify() {
	if !config.MinifyOutput {
		return
	}

	const htmlMimetype = "text/html"
	const cssMimetype = "text/css"
	const xmlMimetype = "text/xml"
	const svgMimetype = "image/svg+xml"

	extMap := map[string]string{
		".html": htmlMimetype,
		".css":  cssMimetype,
		".xml":  xmlMimetype,
	}

	m := minify.New()
	m.AddFunc(htmlMimetype, html.Minify)
	m.AddFunc(cssMimetype, css.Minify)
	m.AddFunc(xmlMimetype, xml.Minify)
	m.AddFunc(svgMimetype, svg.Minify)

	err := filepath.WalkDir(
		config.OutputDir,
		func(path string, entry fs.DirEntry, err error) error {
			if err != nil {
				return err
			}

			if entry.IsDir() {
				return nil
			}

			mimetype, ok := extMap[filepath.Ext(path)]
			if !ok {
				return nil
			}

			buf, fErr := os.ReadFile(path)
			if fErr != nil {
				return fErr
			}
			text := string(buf)

			minifiedText, err := m.String(mimetype, text)
			if err != nil {
				log.Panic(err)
			}

			fErr = os.WriteFile(path, []byte(minifiedText), 0777)
			if fErr != nil {
				return fErr
			}

			return nil
		})
	if err != nil {
		log.Panic(err)
	}
}
