package output

import (
	"log"
	"os"

	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

func CreateDir() {
	err := os.RemoveAll(config.OutputDir)
	if err != nil {
		log.Panic(err)
	}

	err = os.MkdirAll(config.OutputDir, 0777)
	if err != nil {
		log.Panic(err)
	}
}

func CopyStatic() {
	src := os.DirFS("static")
	dest := config.OutputDir
	err := os.CopyFS(dest, src)
	if err != nil {
		log.Panic(err)
	}
}
