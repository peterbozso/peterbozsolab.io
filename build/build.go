package build

import (
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/css"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/feed"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/html"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/md"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/output"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/posts"
)

func Do() {
	output.CreateDir()
	output.CopyStatic()
	md := md.Markdown()
	posts := posts.Parse(md)
	html.Build(posts, md)
	feed.Build(posts)
	css.Build()
	output.Minify()
}
