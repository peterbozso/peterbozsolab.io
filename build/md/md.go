package md

import (
	"log"

	chromahtml "github.com/alecthomas/chroma/v2/formatters/html"
	"github.com/yuin/goldmark"
	highlighting "github.com/yuin/goldmark-highlighting/v2"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/text"
	"github.com/yuin/goldmark/util"
	"go.abhg.dev/goldmark/anchor"
)

func Markdown() goldmark.Markdown {
	return goldmark.New(
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(), // For goldmark-anchor.
			parser.WithASTTransformers(
				util.Prioritized(&linkTransformer{}, 1000),
			),
		),
		goldmark.WithExtensions(
			extension.Typographer, // To transform dumb quotes to smart ones.
			highlighting.NewHighlighting(
				highlighting.WithFormatOptions(
					chromahtml.WithClasses(true),
				),
			),
			&anchor.Extender{
				Texter: &customTexter{},
			},
		),
	)
}

type linkTransformer struct{}

// To add target="_blank" to links.
// Source: https://github.com/glasskube/glasskube/pull/837
func (g *linkTransformer) Transform(node *ast.Document, reader text.Reader, pc parser.Context) {
	err := ast.Walk(node, func(node ast.Node, entering bool) (ast.WalkStatus, error) {
		if !entering {
			return ast.WalkContinue, nil
		}

		link, isLink := node.(*ast.Link)
		if isLink {
			// No need for a new tab if the link points into the current page.
			if string(link.Destination)[0:1] == "#" {
				return ast.WalkContinue, nil
			}

			// No need for 'noopener' anymore: https://stackoverflow.com/a/50709724
			link.SetAttributeString("target", "_blank")
		}

		return ast.WalkContinue, nil
	})
	if err != nil {
		log.Panic(err)
	}
}

type customTexter struct{}

func (*customTexter) AnchorText(h *anchor.HeaderInfo) []byte {
	if h.Level == 1 {
		return nil
	}
	return []byte("#")
}
