package feed

import (
	"log"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/gorilla/feeds"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build/posts"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

func Build(posts []posts.Post) {
	feed := &feeds.Feed{
		Title:   config.BlogTitle,
		Link:    &feeds.Link{Href: config.BlogURL()},
		Created: asTime(posts[0].Date),
		Author:  &feeds.Author{Name: config.AuthorName, Email: config.AuthorEmail},
	}

	for _, post := range posts {
		feed.Items = append(feed.Items, &feeds.Item{
			Title:   post.Title,
			Link:    &feeds.Link{Href: getLink(post)},
			Created: asTime(post.Date),
			Content: string(post.Content),
		})
	}

	atom, err := feed.ToAtom()
	if err != nil {
		log.Panic(err)
	}

	file, err := os.Create(filepath.Join(config.OutputDir, "feed.xml"))
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	_, err = file.WriteString(atom)
	if err != nil {
		log.Panic(err)
	}
}

func getLink(post posts.Post) string {
	link, err := url.JoinPath(config.BlogURL(), post.URL)
	if err != nil {
		log.Panic(err)
	}
	return link
}

func asTime(date string) time.Time {
	time, err := time.Parse(time.DateOnly, date)
	if err != nil {
		log.Panic(err)
	}
	return time
}
