package dev

import (
	"log"
	"net"
	"net/http"
	"os"

	"gitlab.com/peterbozso/peterbozso.gitlab.io/config"
)

func Serve() {
	listener, err := net.Listen("tcp", config.ServeAddress)
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Dev server is listening on: http://%s", config.ServeAddress)

	http.Handle("/", http.FileServer(HTMLDir{http.Dir(config.OutputDir)}))
	http.Serve(listener, nil)
}

type HTMLDir struct {
	d http.Dir
}

// To replicate GitLab Pages' serving behavior.
// Docs: https://docs.gitlab.com/ee/user/project/pages/introduction.html#resolving-ambiguous-urls
// Source: https://stackoverflow.com/a/57281956/3078296
func (d HTMLDir) Open(name string) (http.File, error) {
	f, err := d.d.Open(name)
	if os.IsNotExist(err) {
		f, err := d.d.Open(name + ".html")
		if err == nil {
			return f, nil
		} else {
			f, err = d.d.Open("404.html")
			if err == nil {
				return f, nil
			}
		}
	}
	return f, err
}
