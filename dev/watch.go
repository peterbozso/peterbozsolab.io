package dev

import (
	"io/fs"
	"log"
	"path/filepath"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/build"
)

func WatchRebuild() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Panic(err)
	}
	defer watcher.Close()

	addDirsToWatch(watcher)

	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}

			if event.Has(fsnotify.Chmod) {
				continue
			}

			ext := filepath.Ext(event.Name)
			if ext != ".go" && ext != ".DS_Store" {
				log.Printf("A file (%s) has changed (Operation: %s). Rebuilding site...", event.Name, event.Op)
				build.Do()
				log.Println("Build done.")
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			log.Println("Watch error:", err)
		}
	}
}

func addDirsToWatch(watcher *fsnotify.Watcher) {
	watchDirs := []string{"build", "content"}

	for _, path := range watchDirs {
		err := filepath.WalkDir(
			path,
			func(path string, entry fs.DirEntry, err error) error {
				if err != nil {
					return err
				}

				if entry.IsDir() {
					watchErr := watcher.Add(path)
					if watchErr != nil {
						return watchErr
					}
				}
				return nil
			})
		if err != nil {
			log.Panic(err)
		}
	}
}
