package config

import (
	"fmt"
	"os"
)

const (
	BlogTitle   = "Péter Bozsó's blog"
	AuthorName  = "Péter Bozsó"
	AuthorEmail = "peter.bozso@outlook.com"

	OutputDir    = "public"
	MinifyOutput = true

	// Preview: https://swapoff.org/chroma/playground/
	SyntaxHighlightStyleLight = "monokailight"
	SyntaxHighlightStyleDark  = "monokai"

	ServeAddress = "localhost:3000"
)

func BlogURL() string {
	blogURL := os.Getenv("BLOG_URL")
	if blogURL == "" {
		return fmt.Sprintf("http://%s", ServeAddress)
	}
	return blogURL
}
