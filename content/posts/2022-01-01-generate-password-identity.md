# Generating passwords in ASP.NET Core Identity

While working on an ASP.NET Core project, I bumped into the following challenge: I had to implement a feature that makes it possible for an admin user to create new, non-admin users for a website. To achieve this, I had to create new users with random passwords that would comply with all the requirements configured in [ASP.NET Core Identity's options.][1]

**Please note that creating users manually and generating passwords for them could pose a security threat to your application if you don't have a secure way of giving these passwords to your users. (Sending their passwords in an email is not secure.) For anything beyond home usage, consider using [account registration confirmation with email][2] or use a managed solution like [Azure Active Directory.][3]**

After searching for a while, I realized that there is no built-in way to do this in ASP.NET Core Identity. I also stumbled upon [this Stack Overflow answer.][4]

I really liked the idea presented by this piece of code and felt that this is such a basic feature (which classic ASP.NET actually has in the form of [System.Web.Security.Membership.GeneratePassword][5]) that I decided to create a library that would implement the very same thing in a more robust manner.

That's how the [IdentityPasswordGenerator NuGet package][6] came to be. (Complete source code can be found [here.][7]) Compared to the code on Stack Overflow, I introduced the `excludeNonRequiredChars` parameter, so the library can be used in a more flexible manner, for example, for generating one-time passwords or authentication tokens. I also added some validation logic for the arguments to make sure that the password generation logic cannot get stuck in an infinite loop.

I hope to achieve two things by providing this for the .NET community:

1. Nobody needs to reimplement the same logic every time they need this feature in their applications.

2. By making the code open source, make it possible for people to learn from the way I implemented this fairly simple but security-wise quite sensitive piece of logic.

[1]: https://docs.microsoft.com/en-us/aspnet/core/security/authentication/identity-configuration?view=aspnetcore-6.0#password
[2]: https://docs.microsoft.com/en-us/aspnet/core/security/authentication/accconfirm?view=aspnetcore-6.0&tabs=visual-studio
[3]: https://docs.microsoft.com/en-us/aspnet/core/security/authentication/azure-active-directory/?view=aspnetcore-6.0
[4]: https://stackoverflow.com/questions/28480167/asp-net-identity-generate-random-password/46229180#46229180
[5]: https://docs.microsoft.com/en-us/dotnet/api/system.web.security.membership.generatepassword?view=netframework-4.8#System_Web_Security_Membership_GeneratePassword_System_Int32_System_Int32_
[6]: https://www.nuget.org/packages/IdentityPasswordGenerator/
[7]: https://github.com/peterbozso/IdentityPasswordGenerator
