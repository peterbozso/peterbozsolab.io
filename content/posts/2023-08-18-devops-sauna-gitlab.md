# DevOps Sauna from Eficode

Last week, I had the privilege of being invited as a guest to [Eficode][1]'s [DevOps Sauna podcast][2]. It was great fun recording the episode, so I hope you'll enjoy listening to it equally!

For links to the episode on different streaming platforms and show notes, please visit [the episode's website][3].

[1]: https://www.eficode.com/
[2]: https://www.eficode.com/devops-podcast
[3]: https://www.eficode.com/devops-podcast/from-hobby-projects-to-enterprises-with-gitlab
