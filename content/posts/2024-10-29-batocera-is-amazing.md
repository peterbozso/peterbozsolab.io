# Batocera is amazing

That's right, it is. [Batocera.linux][1] is a truly amazing open-source project.

A friend of mine set a trap for me this summer, mentioning that he had discovered an interesting Linux distribution for retro gaming. He had a lot of fun with it and recommended I give it a try too. I was rather busy at the time, it seemed like a lot of work to get it running, so I put it on my to-do list and, of course, largely forgot about it. But when I visited him a couple of weeks later, I had the chance to actually try it out. I was immediately blown away by how cool this operating system is. Quite obviously, the first thing I did after coming home was to get my old Lenovo T480 out of its hiding place (or rather, graveyard) in my office cabinet and install Batocera on it.

Since then, I've spent an embarrassing amount of time playing retro and indie video games. Hello, [Archon Ultra][2] and [Puzzle Bobble 2][3], my dear old friends! Also, greetings to you, [Nidhogg 2][4] and [Risk of Rain Returns][5], my newfound loves!

Of course, as with every open-source project, the codebase is only a single part of it. Batocera is amazing not just because it can emulate almost any video game system you can think of, but because of the awesome community around it. When I faced some [issues with my Bluetooth soundbar][6], I had the best experience engaging with the core developers on Discord. This motivated me to contribute to the project. With their help, I was able to:

- [update][7] the built-in rclone version and [update][8] rclone's own docs,
- [clean up][9] and [document][10] the batocera-services script,
- then, combining those two, [create a service][11] to back up user data to cloud storage.

My goal with listing all these contributions is not to brag, but to demonstrate how much a couple of pleasant interactions in a healthy community can mean for such projects.

What also blew me away is how easy it is to run Windows games on Batocera. Of course, it's less the merit of the Batocera team and more the result of Valve creating [Proton][12], building it on top of [Wine][13], to support their Steam Deck. It's worth mentioning though as another great example of how big an impact open-source software can have on the wider developer and gaming communities, as well as on society at large. Wine itself is not an immature project by any means, but the amount of resources that Valve dedicated to it definitely took its accessibility (through Proton) to the next level.

I hope this is just the start of my journey with Batocera, both as a user and a developer. If you like retro and indie video games and would like to have a nicely organized, easily maintainable digital collection of them, then I highly recommend giving this operating system a go! It definitely needs a bit of tweaking to get it up and running exactly the way you'd like, but as I've mentioned above, the community is happy to help. Not to mention that the documentation is also one of the best among open-source projects I've ever seen.

Long live Batocera!

[1]: https://batocera.org/
[2]: https://en.wikipedia.org/wiki/Archon_Ultra
[3]: https://en.wikipedia.org/wiki/Puzzle_Bobble_2
[4]: https://en.wikipedia.org/wiki/Nidhogg_2
[5]: https://en.wikipedia.org/wiki/Risk_of_Rain
[6]: https://github.com/batocera-linux/batocera.linux/issues/12247
[7]: https://github.com/batocera-linux/batocera.linux/pull/12371
[8]: https://github.com/rclone/rclone/pull/8016
[9]: https://github.com/batocera-linux/batocera.linux/pull/12370
[10]: https://wiki.batocera.org/launch_a_script#services
[11]: https://github.com/peterbozso/batocera-backup-service
[12]: https://en.wikipedia.org/wiki/Proton_(software)
[13]: https://en.wikipedia.org/wiki/Wine_(software)
