# The Azure Podcast - Episode 255 - Chatbots

Last week, I had the privilege of being invited as a guest to the Azure Podcast. It was great fun recording the episode, so I hope you'll enjoy listening to it equally! For show notes and mp3 download, please visit [the Azure Podcast's website][1].

You can also find the podcast on many platforms, like [iTunes][2] or [Spotify][3].

[1]: http://www.azpodcast.com/post/Episode-255-Chatbots
[2]: https://itunes.apple.com/hu/podcast/the-azure-podcast/id728193635?mt=2#episodeGuid=http%3A%2F%2Fazpodcast.azurewebsites.net%2Fpost.aspx%3Fid%3D9054caac-3e53-4bfb-8bc2-dd3c2a63b00d
[3]: https://open.spotify.com/episode/74wcaVpr8KSuy8d63ciovO?si=9_WGFO-bQDC1aGgYRIa1cQ
