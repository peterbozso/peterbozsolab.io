# About me

![Péter Bozsó's picture](/assets/about-profile-picture.jpg)

My name is Péter Bozsó. I am a [Hungarian][1] living in [Vienna][2], and I work as an [Ecosystem Solutions Architect][3] at [GitLab][4].

This is my personal blog about everything that interests me. Opinions are my own and not the views of my employer. My main interests are software development, retro and indie video games, spending time in nature, music, history, sociology, and politics. You'll most likely find content about these topics here.

You can find the full content and the source code of this website in [this repository][5]. If you have questions or feedback about either of those, please open an issue [here][6]. Before reusing any materials, please consult the [license notice][7].

To get in touch, feel free to [send me an email][8] or [contact me on LinkedIn][9].

[1]: https://en.wikipedia.org/wiki/Hungarians
[2]: https://en.wikipedia.org/wiki/Vienna
[3]: https://handbook.gitlab.com/job-families/sales/solutions-architect/#ecosystem-solutions-architect
[4]: https://about.gitlab.com/
[5]: https://gitlab.com/peterbozso/peterbozso.gitlab.io
[6]: https://gitlab.com/peterbozso/peterbozso.gitlab.io/-/issues
[7]: https://gitlab.com/peterbozso/peterbozso.gitlab.io#license
[8]: mailto:peter.bozso@outlook.com
[9]: https://www.linkedin.com/in/peterbozso/
