package main

import (
	"os"

	"gitlab.com/peterbozso/peterbozso.gitlab.io/build"
	"gitlab.com/peterbozso/peterbozso.gitlab.io/dev"
)

func main() {
	switch os.Args[1] {
	case "build":
		build.Do()
	case "serve":
		build.Do()
		go dev.WatchRebuild()
		dev.Serve()
	}
}
